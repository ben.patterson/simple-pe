================
simple_pe_hm_amp
================

The `simple_pe_hm_amp` executable calculates the relative amplitude of 
the specified higher modes, and their overlap with the (2, 2) mode, over 
the given region of mass and mass ratio space.

   
 To see help for this executable please run:

.. code-block:: console

    $ simple_pe_hm_amp --help

.. program-output:: simple_pe_hm_amp --help