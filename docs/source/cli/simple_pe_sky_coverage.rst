======================
simple_pe_sky_coverage
======================

The `simple_pe_sky_coverage` executable allows the user
to calculate the sensitivity of a gravitational wave detector
network to sources from different sky locations.
The code generates two figures, one which shows the network sensitivity
over the sky and the second which shows the relative sensitivity of the 
network to the second GW polarization.

To see help for this executable please run:

.. code-block:: console

    $ simple_pe_sky_coverage --help

.. program-output:: simple_pe_sky_coverage --help

Below is a plot showing network sensitivity for the advanced LIGO,
Virgo network at design, and also a plot of the relative
sensitivity to the second polarization for the network over the sky.

.. image:: ../images/design_sky_sens.png

.. image:: ../images/design_2nd_pol.png