===============================
simple_pe_localization_ellipses
===============================

The `simple_pe_localization_ellipses` executable allows the user
to generate a set of face-on binaries at a fixed distance on a grid 
over the sky and evaluate the localization accuracy for each event.
The results are plotted as a set of ellipses in the sky.

The localization can be performed in one of several different ways:

* 'time': using time delays between detectors only
* 'coh': requiring a coherent signal between the detectors
* 'left': assuming that the signal is left circular polarized
* 'right': assuming that the signal is right circular polarized
* 'marg': marginalizing the likelihood over distance and orientation to determe which of
  ('coh','left','right') gives the highest likelihood and use that in localization.
   
To see help for this executable please run:

.. code-block:: console

    $ simple_pe_localization_ellipses --help

.. program-output:: simple_pe_localization_ellipses --help

Below is a  plot showing localization ellispes for the default configuration:

* advanced LIGO, Virgo detectors at design sensitivity
* SNR thresholds of 5 in each detector and 12 in the network, 
  with a requirement of SNR > 4 for a detector to contribute to localization
* a grid of 16x16 sky points with signals at a distance of 220 Mpc

The output is the following figure:

.. image:: ../images/design_sky_ellipses_found.png