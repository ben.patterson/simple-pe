from astropy.cosmology import Planck18 as cosmo
from astropy.cosmology import z_at_value
import astropy.units as u
import numpy as np
from scipy import interpolate


max_z = 400.
redshifts = np.linspace(0, max_z, int(max_z*10))
distances = cosmo.comoving_distance(redshifts)
dl = cosmo.luminosity_distance(redshifts)
z_interp = interpolate.interp1d(distances, redshifts)
zdl_interp = interpolate.interp1d(dl, redshifts)


def redshift_at_comoving_dist(distance):
    """
    Return redshift at a given comoving distance

    :param distance: comoving distance
    :type distance: np.array, required

    :return: redshift
    :rtype: np.array
    """
    return z_interp(distance)


def redshift_at_lum_dist(distance):
    """
    Return redshift at a given luminosity distance

    :param distance: luminosity distance
    :type distance: np.array, required

    :return: redshift
    :rtype: np.array
    """
    return zdl_interp(distance)


def age_at_redshift(z):
    """
    Return age (in billions of years) at a given redshift

    :param t: time in billions of years
    :type t: np.array, required

    :return: redshift
    :rtype: np.array
    """
    t = cosmo.age(z)/u.Gyr
    return t


age_at_z = np.array([cosmo.age(z)/u.Gyr for z in redshifts])
age_at_z_interp = interpolate.interp1d(redshifts, age_at_z)
epsilon = 0.0005
ages = np.linspace(epsilon, age_at_redshift(0)-epsilon, 1000)
z_at_ages = np.array([z_at_value(cosmo.age, age*u.Gyr) for age in ages])
ages = np.append(ages, age_at_redshift(0))
z_at_ages = np.append(z_at_ages, 0)
z_at_age_interp = interpolate.interp1d(ages, z_at_ages)


def redshift_at_age(t):
    """
    Return redshift at a given age (in billions of years)

    :param t: time in billions of years
    :type t: np.array, required

    :return: redshift
    :rtype: np.array
    """
    z = z_at_age_interp(t)
    return z


def luminosity_distance(distance):
    """
    return luminosity distance for a given comoving distance

    :param distance: comoving distance
    :type distance: np.array, required

    :return: the luminosity_distance
    :rtype: np.array
    """
    return (1 + z_interp(distance))*distance/u.Mpc


def volume(z_list, ratio_list):
    """
    return volume at a set of redshifts

    :param z_list: set of redshifts
    :type z_list: np.array, required
    :param ratio_list: list of volume ratios
    :type ratio_list: np.array,required

    :return: the weighted differential comoving volume
    :rtype: np.array
    """
    return (4 * np.pi * ratio_list *
            cosmo.differential_comoving_volume(z_list).value/(1+z_list)).sum()
