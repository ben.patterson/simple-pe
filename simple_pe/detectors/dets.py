import numpy as np
from pycbc import detector
from simple_pe.detectors import noise_curves
from pesummary.gw.conversions.mass import mchirp_from_m1_m2


##################################################################
# Class to store detector information
##################################################################
class Det(detector.Detector):
    """
    Class to hold the details of a detector.
    Inherits from pycbc.detector.Detector class.

    """
    def __init__(self, detector_name, horizon, f_mean, f_band,
                 found_thresh=5.0, loc_thresh=4.0, duty_cycle=1.0,
                 bns_range=True, psd=None, f_low=None, approximant=None):
        """
        :param detector_name: 2 character string for detector
        :type detector_name: str, required
        :param horizon: the BNS horizon of the detector
        :type horizon: float, required
        :param f_mean: mean frequency
        :type f_mean: float, required
        :param f_band: frequency bandwidth
        :type f_band: float, required
        :param found_thresh: threshold for declaring an event found
        :type found_thresh: float, required
        :param loc_thresh: threshold for declaring an event localized
        :type loc_thresh: float, optional
        :param duty_cycle: fraction of time the detector is operational
        :type duty_cycle: float, optional
        :param bns_range: is the given range for BNS
          (if yes, then rescale SNR with mchirp^5/6)
        :type bns_range: boolean, optional

        :param psd: the PSD
        :type psd: pycbc.psd, optional
        :param f_low: the low frequency cutoff
        :type f_low: float, optional
        :param approximant: the waveform approximant to use
            in generating sensitivity
        :type approximant: str, optional
        """
        super().__init__(detector_name)
        self.horizon = horizon
        self.sigma = horizon * 8  # this gives the SNR at 1 Mpc
        self.f_mean = f_mean
        self.f_band = f_band
        self.found_thresh = found_thresh
        self.loc_thresh = loc_thresh
        self.duty_cycle = duty_cycle
        self.bns_range = bns_range
        self.psd = None
        self.f_low = None
        self.approximant = None

    @classmethod
    def from_psd(cls, detector_name, psd, f_low,
                 approximant="IMRPhenomD",
                 found_thresh=5.0,
                 loc_thresh=4.0, duty_cycle=1.0,
                 bns_range=True):
        """
        Give a PSD to initialize the detector

        :param detector_name: 2 character string for detector
        :type detector_name: str, required
        :param psd: PSD to use in calculating sensitivity
        :type psd: pycbc.psd, required
        :param f_low: the low frequency cutoff to use
        :type f_low: float, required
        :param approximant: the waveform approximant
        :type approximant: str, optional
        :param found_thresh: threshold for declaring an event found
        :type found_thresh: float, required
        :param loc_thresh: threshold for declaring an event localized
        :type loc_thresh: float, optional
        :param duty_cycle: fraction of time the detector is operational
        :type duty_cycle: float, optional
        :param bns_range: is the given range for BNS
          (if yes, then rescale SNR with mchirp^5/6)
        """
        masses = [1.4, 1.4]
        spin = 0.
        horizon, f_mean, f_band = \
            noise_curves.calc_reach_bandwidth(masses, spin, approximant,
                                              psd, f_low, thresh=8.,
                                              mass_configuration="component"
                                             )
        return cls(detector_name, horizon, f_mean, f_band,
                   found_thresh, loc_thresh, duty_cycle,
                   bns_range, psd, f_low
                  )

    def calculate_sensitivity(self, event):
        """
        Calculate the sensitivity of the detector to an event

        :param event: object, containing ra, dec, psi, gmst
        :type event: event.Event
        """
        return self.antenna_pattern(event.ra, event.dec,
                                    event.psi, event.gps)

    def calculate_mirror_sensitivity(self, event):
        """
        Calculate the sensitivity of the detector to an event,
        in its mirror sky location

        :param event: object, containing ra, dec, psi, gmst
        :type event: event.Event
        """
        return self.antenna_pattern(event.mirror_ra, event.mirror_dec,
                                    event.psi, event.gps)

    def calculate_snr(self, event):
        """
        Calculate the expected SNR of the event in the detector

        :param event: object, containing ra, dec, psi, gmst, cosi, phi
        :type event: event.Event

        :return: the complex SNR for the signal
        :rtype: complex
        """
        f_plus, f_cross = self.calculate_sensitivity(event)
        if self.bns_range:
            mass_scale = (event.mchirp /
                          mchirp_from_m1_m2(1.4, 1.4)) ** (5. / 6)
        else:
            mass_scale = 1.
        snr = mass_scale * self.sigma / event.D * \
            complex(np.cos(2 * event.phi), - np.sin(2 * event.phi)) * \
            complex(f_plus * (1 + event.cosi ** 2) / 2,
                    f_cross * event.cosi)
        return snr

    def get_fsig(self, event, mirror=False):
        """
        Method to return the sensitivity of the detector

        :param mirror: is this the mirror position
        :type mirror: boolean

        :return: the detector sensitivity to the two polarizations,
            sigma * (F_plus, F_cross)
        :rtype: np.array
        Returns
        """
        if mirror:
            return self.sigma * \
                np.array(self.calculate_mirror_sensitivity(event))
        else:
            return self.sigma * np.array(self.calculate_sensitivity(event))
