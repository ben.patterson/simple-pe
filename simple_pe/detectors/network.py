import numpy as np
from simple_pe import detectors
from simple_pe.detectors import Det


class Network(object):
    """
    Class to hold the details of the network.
    """

    def __init__(self, threshold=12.0):
        """
        Initialize the Network object

        :param threshold: SNR detection threshold for a network,
          defaults to 12.0
        :type threshold: float, optional
        """
        self.threshold = threshold
        self.ifos = []

    def add_ifo(self, ifo, horizon, f_mean, f_band,
                found_thresh=5.0, loc_thresh=4.0, duty_cycle=1.0,
                bns_range=True):
        """
        Add an ifo into a network

        :param ifo: name of ifo
        :type ifo: str
        :param horizon: the BNS range of the detector
        :type horizon: float
        :param f_mean: mean frequency
        :type f_mean: float
        :param f_band: frequency bandwidth
        :type f_band: float
        :param found_thresh: threshold for declaring an event found,
          defaults to 5.0
        :type found_thresh: float, optional
        :param loc_thresh: threshold for declaring an event localized,
          defaults to 4.0
        :type loc_thresh: float, optional
        :param duty_cycle: fraction of time the detector is operational,
          defaults to 1.0
        :type duty_cycle: float, optional
        :param bns_range: is the given range for BNS
          (if yes, then rescale SNR with chirp_mass^5/6),
          defaults to True
        :type bns_range: bool, optional
        """

        d = Det(ifo, horizon, f_mean, f_band,
                found_thresh, loc_thresh, duty_cycle, bns_range)
        setattr(self, ifo, d)
        self.ifos.append(ifo)

    def add_ifo_from_psd(self, ifo, psd, f_low, approximant='IMRPhenomD',
                         found_thresh=5.0, loc_thresh=4.0, duty_cycle=1.0,
                         bns_range=True):
        """
        Add an ifo to a network using the PSD

        Parameters
        ----------
        ifo: str
            name of ifo
        psd: pycbc.psd
            the psd to use
        f_low: float
            the low frequency cutoff
        approximant: str
            approximant to use in waveform generation           
        found_thresh: float
            threshold for declaring an event found
        loc_thresh: float
            threshold for declaring an event localized
        duty_cycle: float
            fraction of time the detector is operational
        bns_range: float
            is the given range for BNS (if yes, then rescale SNR with
            chirp_mass^5/6)
        """
        d = Det.from_psd(ifo, psd, f_low, approximant,
                found_thresh, loc_thresh, duty_cycle, bns_range)
        setattr(self, ifo, d)
        self.ifos.append(ifo)

    def set_configuration(self, configuration, found_thresh=5.0,
                          loc_thresh=4.0, duty_cycle=1.0):
        """
        set the details of the detectors based on the given configuration.
        data is stored in the detectors module

        :param configuration: name of configuration
        :type configuration: str, required
        :param found_thresh: threshold for single ifo detection
        :type found_thresh: float, optional
        :param loc_thresh: threshold for single ifo localization
        :type loc_thresh: float, optional
        :param duty_cycle: fraction of time detectors are operational
        :type duty_cycle: float, optional
        """
        ranges = detectors.range_8(configuration)
        ifos = ranges.keys()
        fmeans = detectors.fmean(configuration)
        fbands = detectors.bandwidth(configuration)
        for ifo in ifos:
            self.add_ifo(ifo, 2.26 * ranges[ifo], fmeans[ifo], fbands[ifo],
                         found_thresh, loc_thresh, duty_cycle)

    def generate_network_from_psds(self, ifos, psds, f_lows,
                                   approximant='IMRPhenomD',
                                   found_thresh=5.0, loc_thresh=4.0,
                                   duty_cycle=1.0, bns_range=True):
        """
        Generate a network from a list of ifos, with associated PSDs and f_lows

        Parameters
        ----------
        ifos: list
            A list of ifos
        psds: dict
            Dictionary of psds associated with the ifos
        f_lows: dict
            Dictionary of low frequency cutoffs for each ifo
        approximant: str
            approximant to use in waveform generation
        found_thresh: float
            threshold for declaring an event found
        loc_thresh: float
            threshold for declaring an event localized
        duty_cycle: float
            fraction of time the detector is operational
        bns_range: bool
            is the given range for BNS (if yes, then rescale SNR with
            chirp_mass^5/6)
        """
        for ifo in ifos:
            self.add_ifo_from_psd(ifo, psds[ifo], f_lows[ifo], approximant, 
                                  found_thresh, loc_thresh, duty_cycle,
                                  bns_range)

    def get_data(self, data):
        """
        get the relevant data for each detector and return it as an array

        :param data: name of data to return from a detector
        :type data:str

        :return: an array containing the data
        :rtype: np.array
        """
        return np.array([getattr(getattr(self, i), data) for i in self.ifos])
