import numpy as np
import pylab as plt
import os
import argparse

from pycbc import conversions, psd, strain, waveform
from simple_pe import waveforms


def command_line():
    """Define the command line arguments for `simple_pe_localization_ellipses`
    """
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument(
        "--psd",
        help=(
            "PSD specification to use for the analysis. "
            "Available options given by psd.get_lalsim_psd_list()"
        ),
        default='aLIGOaLIGODesignSensitivityT1800044',
        type=str,
        action='store',
    )
    parser.add_argument(
        "--f_low",
        help=(
            "low frequency cutoff to be used."
        ),
        default=20.,
        type=float,
        action='store',
    )
    parser.add_argument(
        "--mode",
        help="gravitational wave multipole to evaluate (can be more than one)",
        dest='modes',
        default=[],
        type=str,
        action='append',
    )
    parser.add_argument(
        "--min-mtotal",
        help="Minimum total mass (Msun) to use ",
        default=10.0,
        type=float,
    )
    parser.add_argument(
        "--max-mtotal",
        help="Maximum total mass (Msun) to use ",
        default=200.0,
        type=float,
    )
    parser.add_argument(
        "--min-mass-ratio",
        help="Minimum mass ratio",
        default=0.3,
        type=float,
    )
    parser.add_argument(
        "--max-mass-ratio",
        help="Maximum mass ratio",
        default=0.99,
        type=float,
    )
    parser.add_argument(
        "--spin1z",
        help="Value of spin1 z-component",
        default=0.0,
        type=float,
    )
    parser.add_argument(
        "--spin2z",
        help="Value of spin2 z-component",
        default=0.0,
        type=float,
    )
    parser.add_argument(
        "--approximant",
        help="Approximant to use for the analysis",
        default="IMRPhenomXPHM",
    )
    parser.add_argument(
        "--npoints",
        help="Number of points in total mass--mass ratio dimensions to use",
        default=16,
        type=int,
    )
    parser.add_argument(
        "--outdir",
        help="Directory to store the output",
        default="./",
    )

    return parser


def main(args=None):
    """
    Main interface for `simple_pe_hp_amp`
    """
    parser = command_line()
    opts, _ = parser.parse_known_args(args=args)

    if not os.path.isdir(opts.outdir):
        os.mkdir(opts.outdir)

    m1 = m2 = opts.min_mtotal/2
    f_high = 4096
    length = strain.strain.next_power_of_2(
        int(waveform.compress.rough_time_estimate(m1, m2, float(opts.f_low))))
    ifo_psd = psd.analytical.from_string(opts.psd, length * f_high + 1,
                                         1/length, opts.f_low)

    masses, qs = \
        np.mgrid[opts.min_mtotal:opts.max_mtotal:1j*opts.npoints,
                 opts.min_mass_ratio:opts.max_mass_ratio:1j*opts.npoints]

    alphas = {}
    overlaps = {}
    for mode in opts.modes:
        alphas[mode] = np.zeros_like(masses)
        overlaps[mode] = np.zeros_like(masses)

    for i, mass in np.ndenumerate(masses):
        alpha_lm, overlap_lm = \
            waveforms.calculate_alpha_lm_and_overlaps(
                conversions.mass1_from_mtotal_q(mass, qs[i]),
                conversions.mass2_from_mtotal_q(mass, qs[i]),
                opts.spin1z, opts.spin2z, ifo_psd, opts.f_low,
                opts.approximant, opts.modes)
        for mode in opts.modes:
            alphas[mode][i] = alpha_lm[mode]
            overlaps[mode][i] = overlap_lm[mode]

    for mode in opts.modes:
        plt.figure(figsize=(10, 6))
        plt.contourf(masses, qs, alphas[mode], levels=20)
        plt.colorbar()
        plt.xlabel("Total mass (Msun)")
        plt.ylabel("Mass ratio")
        plt.title("Relative amplitude of mode (%s, %s)" % (mode[0], mode[1]))
        plt.savefig('%s/hm_amp_%s.png' % (opts.outdir, mode),
                    bbox_inches='tight')

        plt.figure(figsize=(10,6))
        plt.contourf(masses, qs, overlaps[mode], levels=20)
        plt.colorbar()
        plt.xlabel("Total mass (Msun)")
        plt.ylabel("Mass ratio")
        plt.title("Overlap of mode (%s, %s) with (2, 2)" % (mode[0], mode[1]))
        plt.savefig('%s/hm_overlap_%s.png' % (opts.outdir, mode), 
                bbox_inches='tight')

if __name__ == "__main__":
    main()
